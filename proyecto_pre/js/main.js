import { scrollActiv, setRRSSLinks } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isMobile, isElementInViewport } from '../../common_projects_utilities/js/dom-helpers';
//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

//Ejecución
let viewportHeight = window.innerHeight;
let footer = document.getElementById("footerReference");
let sharing = document.getElementById("sharing");
let summary = document.getElementsByClassName("content--summary");
let summaryArr = Array.prototype.slice.call(summary);

document.addEventListener("DOMContentLoaded", function(event) {
	setRRSSLinks();
});

window.addEventListener('scroll', function(){
	scrollActiv();

	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
	if ( scrollTop > viewportHeight ){
		sharing.classList.add("visible")
	} else {
		sharing.classList.remove("visible")
	}
	
	if (isMobile()) {
		if (isElementInViewport(footer)) {
			sharing.classList.remove("visible");
		}
	}

	for(let i = 0; i < summaryArr.length;i++) {
		if(isElementInViewport(summaryArr[i])) {
			if(!summaryArr[i].classList.contains('visible')) {
				summaryArr[i].classList.add('visible');
			}
		}
	}
});